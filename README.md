Base CSS template
=================

A starter CSS template to cover common HTML elements.

The idea is that we ask the designer to modify to a matching design file (Photoshop, Illustrator or InDesign) 
then we apply all the styling in CSS using this template.

This forms the foundation of the main CSS file on a new site, and (hopefully) avoids any surprises later in the project.

The CSS is compiled from a collection of LESS files:
* reset.less - a CSS reset from http://meyerweb.com/eric/tools/css/reset/ 
* grid.less - My current favourite grid system from http://semantic.gs/
* fonts.less - contains a few useful alert icons and a collection of social network icons from https://icomoon.io/
* styles.less - Imports all of the above. This is the one you edit.

